//
//  TableViewController.swift
//  CursoCoreData
//
//  Created by Macbook Pro on 06/01/20.
//  Copyright © 2020 Macbook Pro Oscar. All rights reserved.
//

import UIKit
import CoreData

class TableViewController: UITableViewController {
    
    var manageObjects:[NSManagedObject] = []
    var list:[String] = ["Mérida", "Puebla", "Iztapalapa"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Llamado a appdelagate
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "List")
        
        do{
            manageObjects = try managedContext.fetch(fetchRequest)
        }catch let error as NSError{
            print("\(error.userInfo)")
        }
        
    }
    
    
    @IBAction func addWord(_ sender: Any) {
        let alert = UIAlertController(title: "Nueva Palabra", message: "Agregar nueva palabra", preferredStyle: .alert)
        let guardar = UIAlertAction(title: "Agregar", style: .default, handler: {
            (action:UIAlertAction) -> Void in
            
            let textField = alert.textFields![0]
            let textFieldAge = alert.textFields![1]
            //self.list.append(textField!.text!)
            
                self.saveWord(word: textField.text!, age: Int16(textFieldAge.text!)!)
                self.tableView.reloadData()
                
            
        })
        
        let cancelar = UIAlertAction(title: "Cancelar", style: .default)
        {(action: UIAlertAction) -> Void in }
        
        alert.addTextField {(textField:UITextField) -> Void in}
        alert.addTextField {(textFieldAge:UITextField) -> Void in}
        
            alert.addAction(guardar)
            alert.addAction(cancelar)
            
            present (alert, animated: true, completion: nil)
        
    }
    
    func saveWord(word:String, age:Int16) {
        if age < 12 || age > 100 {
            print("Valió")
        }else{
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let managedContext = appDelegate!.persistentContainer.viewContext
            
            let entity = NSEntityDescription.entity(forEntityName: "List", in: managedContext)!
            let managedObject = NSManagedObject(entity: entity, insertInto: managedContext)
            
            managedObject.setValue(word, forKeyPath: "word")
            managedObject.setValue(age, forKeyPath: "age")
            
            do{
                try managedContext.save()
                manageObjects.append(managedObject)
            }catch let error as NSError{
                print("\(error.userInfo)")
            }
        }
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return manageObjects.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let obj = manageObjects[indexPath.row]
        var edad = obj.value(forKey: "age")!
        var nombre = obj.value(forKey: "word")!
        cell.textLabel?.text = "\(nombre) \(edad) años"
        //cell.textLabel?.text = obj.value(forKey: "age") as? String
        //cell.textLabel?.text = list[indexPath.row]
        // Configure the cell...

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //self.list.remove(at: indexPath.row)
            // Delete the row from the data source
            let obj = manageObjects[indexPath.row]
            
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let managedContext = appDelegate!.persistentContainer.viewContext
            managedContext.delete(obj)
            manageObjects.remove(at: indexPath.row)
            
            
            
            
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
